# Restaurant Management System Using Hashmaps
### First you must choose the types of operations you want to execute:
* As an admin
* As a waiter

#### Admin Operations:
* Insert Product: the product can be a base one or a composite one
* Delete Product
* Edit Product

#### Waiter Operations:
* Create Order
* View All Items

# Note: This project uses Composite Design Pattern, Observer Design Pattern, Reflection Techniques.