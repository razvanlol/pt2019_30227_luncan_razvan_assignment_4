package Business;


import java.io.Serializable;

public class BaseProduct implements MenuItem , Serializable {

    private int price;
    private String name;

    public BaseProduct(String name,int price){

        this.name=name;
        this.price=price;

    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price=price;
    }

    public String getName() {
        return this.name;
    }

    public int computePrice() {
        return this.price;
    }

    public void addItem(MenuItem menuItem) {

    }
}
