package Business;

public interface MenuItem {
    public void addItem(MenuItem menuItem);
    public int getPrice();
    public void setPrice(int price);
    public int computePrice();
    public String getName();
}
