package Business;

import java.util.ArrayList;
import java.util.Date;

public interface RestaurantInterface {

    /**
     * @pre isWellFormed()
     * @pre !nume.matches(".*\\d.*")
     * @pre pret>0
     * @post nume.length()<50
     * @post isWellFormed()
     */

    public void addInRestaurant(String nume, int pret, String componente, ArrayList<String> numeComp);

    /**
     *  @pre isWellFormed()
     *  @pre orderID>0
     *  @pre tableID>0
     *  @post order!=null
     *  @post Restaurant.orderInformation.get(order)!=null
     *  @post isWellFormed()
     */

    public void createOrder(int orderID, Date date, int tableID, ArrayList<String> components);

    /**
     * @pre isWellFormed()
     * @post isWellFromed()
     */

    public void deleteFromRestaurant(String nume);

    public int getLimit();

    public int getPriceByName(String name);

    public boolean isFound(String name);

    public MenuItem findByName(String name);

    /**
     * @pre isWellFormed()
     * @pre pret>0
     * @post isWellFormed()
     */

    public void editInRestaurant(String nume,int pret);



}

