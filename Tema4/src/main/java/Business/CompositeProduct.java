package Business;

import java.io.Serializable;
import java.util.ArrayList;

public class CompositeProduct  implements MenuItem, Serializable {

    private ArrayList<MenuItem> menuItems;
    private int price;
    private String name;

    public CompositeProduct(String name,int price){
        this.price=price;
        this.name=name;
        this.menuItems=new ArrayList<MenuItem>();
    }

    public void setPrice(int price){
        this.price=price;
    }

    public void addItem(MenuItem menuItem) {
        String nume=menuItem.getName();
        for (MenuItem menuItem1:this.menuItems){
            if(menuItem1.getName().compareToIgnoreCase(nume)==0){
                System.out.println("This composite product already has such a base product.");
            }
        }
        this.menuItems.add(menuItem);

    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public int computePrice() {
        int sum=0;
        for(MenuItem menuItem:menuItems){
            sum+=menuItem.computePrice();
        }
        return sum;
    }
}
