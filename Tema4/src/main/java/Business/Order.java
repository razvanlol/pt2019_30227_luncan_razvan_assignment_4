package Business;

import java.io.Serializable;
import java.util.Date;

public class Order implements Serializable {

    private int orderID;
    private Date date;
    private int tableID;

    public Order(int orderID,Date date,int tableID){
        this.orderID=orderID;
        this.date=date;
        this.tableID=tableID;
    }

    @Override
    public boolean equals(Object object){

        if(object==null){
            return false;
        }

        if(getClass()!=object.getClass()){
            return false;
        }

        Order compared=(Order) object;

        if(this.orderID!=compared.getOrderID()){
            return false;
        }

        if(this.tableID!=compared.getTableID()){
            return false;
        }

        if(!this.date.equals(compared.getDate())){
            return false;
        }
        return true;
    }

    public String toString(){
        return this.date+" "+this.tableID+" "+this.orderID;
    }

    public Date getDate() {
        return date;
    }

    public int getOrderID() {
        return orderID;
    }

    public int getTableID() {
        return tableID;
    }

    public int hashCode(){
        return this.orderID+this.tableID+(int)this.date.getTime();
    }

}
