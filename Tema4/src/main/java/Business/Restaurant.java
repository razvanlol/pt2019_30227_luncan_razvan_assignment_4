package Business;

import Data.FileHandler;

import java.util.*;

/**
 @Invariant isWellFormed()
 */

public class Restaurant extends Observable implements RestaurantInterface{
    public static HashMap<Order, ArrayList<MenuItem>> orderInformation;
    public static ArrayList<MenuItem> menuItems;
    private int limit;
    private int orderLimit;
    private FileHandler fileHandler;


    public Restaurant(int limit, int orderLimit, FileHandler fileHandler){
        this.orderInformation=new HashMap<Order, ArrayList<MenuItem>>();
        this.menuItems=new ArrayList<MenuItem>();
        this.limit=limit;
        this.orderLimit=orderLimit;
        this.fileHandler=fileHandler;
    }

    public void addInRestaurant(String nume,int pret,String componente,ArrayList<String> numeComp){
        assert isWellFormed():"The Restaurant is not well formed!";
        assert !nume.matches(".*\\d.*") :"Product contains digits!";
        assert pret>0:"The price must be a positive value!";
        if(componente.compareTo("")!=0){
            if(!isFound(nume)){
                BaseProduct baseProduct=new BaseProduct(nume,pret);
                menuItems.add(baseProduct);
            }
        }else{
             CompositeProduct compositeProduct=new CompositeProduct(nume,pret);
             for(String s:numeComp){
                 if(findByName(s)!=null){
                     compositeProduct.addItem(findByName(s));
                 }
             }
             menuItems.add(compositeProduct);
        }
        assert nume.length()<50:"The name of the product must have less than 50 characters.";
        assert isWellFormed():"The Restaurant is not well formed!";
        fileHandler.update();
    }

    public void editInRestaurant(String nume,int pret){
        assert isWellFormed():"The Restaurant is not well formed!";
        assert pret>0:"The price must be a positive value!";
        for(MenuItem menuItem: Restaurant.menuItems){
            if(menuItem.getName().compareToIgnoreCase(nume)==0){
                menuItem.setPrice(pret);
            }
        }
        assert isWellFormed():"The Restaurant is not well formed!";
        fileHandler.update();
    }

    public void createOrder(int orderID,Date date,int tableID,ArrayList<String> components){
        assert isWellFormed():"The Restaurant is not well formed!";
        assert orderID>0:"The order id must be a positive value!";
        assert tableID>0:"The table id must be a positive value!";
        Order order=new Order(orderID,date,tableID);
        ArrayList<MenuItem> iteme=new ArrayList<MenuItem>();
        for(String s:components){
            iteme.add(findByName(s));
        }
        Restaurant.orderInformation.put(order,iteme);
        setChanged();
        notifyObservers(order);
        for(Order order1:Restaurant.orderInformation.keySet()){
            System.out.println(order1);
        }
        assert order!=null:"The order is not a valid one!";
        assert Restaurant.orderInformation.get(order)!=null:"The order does not contain any products!";
        assert isWellFormed():"The Restaurant is not well formed!";
        fileHandler.updateHashMap();
    }

    public void deleteFromRestaurant(String nume){
        assert isWellFormed():"The Restaurant is not well formed!";
        ArrayList<MenuItem>aux= new ArrayList<MenuItem>();
        aux.addAll(Restaurant.menuItems);
        for(MenuItem menuItem:aux){
            if(menuItem instanceof CompositeProduct){
                Restaurant.menuItems.remove(menuItem);
            }
        }
        aux.clear();
        aux.addAll(Restaurant.menuItems);
        for(MenuItem menuItem:aux){
            if(menuItem.getName().compareToIgnoreCase(nume)==0){
                if(menuItem instanceof BaseProduct){
                    Restaurant.menuItems.remove(menuItem);
                }
            }
        }
        assert isWellFormed():"The Restaurant is not well formed!";
        fileHandler.update();
    }


    protected boolean isWellFormed(){

        if(menuItems==null){
            return false;
        }

        if(orderInformation==null){
            return false;
        }

        if(orderLimit>300){
            return false;
        }

        if(this.limit<200 || this.limit>500){
            return false;
        }

        if(orderInformation.keySet().size()>orderLimit){
            return false;
        }

        if(menuItems.size()>this.limit){
            return false;
        }
        return true;
    }


    public static int computePrice(ArrayList<MenuItem> menuItems){
        int price=0;
        for(MenuItem menuItem:menuItems){
            price+=menuItem.getPrice();
        }

        return price;
    }

    public int getLimit(){
        return limit;
    }


    public int getPriceByName(String name){
        for(MenuItem menuItem: menuItems){
            if(menuItem.getName().compareToIgnoreCase(name)==0){
                return menuItem.getPrice();
            }
        }
        return 0;
    }

    public boolean isFound(String name){
        for(MenuItem menuItem:menuItems){
            if(menuItem.getName().compareToIgnoreCase(name)==0){
                return true;
            }
        }
        return false;
    }

    public MenuItem findByName(String name){
        for(MenuItem menuItem:menuItems){
            if(menuItem.getName().compareToIgnoreCase(name)==0){
                return menuItem;
            }
        }
        return null;
    }
}
