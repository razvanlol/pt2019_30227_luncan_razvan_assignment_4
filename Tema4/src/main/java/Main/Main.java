package Main;

import Business.Restaurant;
import Controller.Controller;
import Data.FileHandler;
import Presentation.*;

import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

public class Main {

    public static void main(String[] args) {
        ChooseView chooseView=new ChooseView();
        AdministratorView administratorView=new AdministratorView();
        AdminInsert adminInsert=new AdminInsert();
        WaiterView waiterView=new WaiterView();
        WaiterInsert waiterInsert=new WaiterInsert();
        FileHandler fileHandler=new FileHandler();
        Restaurant restaurant=new Restaurant(250,100,fileHandler);
        Chef chef=new Chef();
        restaurant.addObserver(chef);
        Controller controller=new Controller(adminInsert,waiterInsert,administratorView,waiterView,chooseView,restaurant,chef,fileHandler);
        try
        {
            File file=new File("E://Facultate//An2//SEM2//TP//Teme//Tema4//list.txt");
            FileInputStream fis = new FileInputStream(file);
            try{
                ObjectInputStream ois = new ObjectInputStream(fis);
                Restaurant.menuItems = (ArrayList) ois.readObject();
                ois.close();
            }catch (EOFException eof){
                JFrame jFrame=new JFrame();
                JOptionPane.showMessageDialog(jFrame,"File of list is empty");
            }


            fis.close();
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
            return;
        }
        catch (ClassNotFoundException c)
        {
            System.out.println("Class not found");
            c.printStackTrace();
            return;
        }
        try
        {
            File file=new File("E://Facultate//An2//SEM2//TP//Teme//Tema4//hashmap.txt");
            FileInputStream fis = new FileInputStream(file);
            try{
                ObjectInputStream ois = new ObjectInputStream(fis);
                Restaurant.orderInformation = (HashMap) ois.readObject();
                ois.close();
            }catch (EOFException eof){
                JFrame jFrame=new JFrame();
                JOptionPane.showMessageDialog(jFrame,"File of hashmap is empty");
            }


            fis.close();
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
            return;
        }
        catch (ClassNotFoundException c)
        {
            System.out.println("Class not found");
            c.printStackTrace();
            return;
        }
    }
}
