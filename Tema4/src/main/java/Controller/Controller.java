package Controller;

import Business.*;
import Data.FileHandler;
import Presentation.*;
import Presentation.Chef;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class Controller {
    private AdminInsert adminInsert;
    private WaiterInsert waiterInsert;
    private AdministratorView administratorView;
    private WaiterView waiterView;
    private ChooseView chooseView;
    private Restaurant restaurant;
    private FileHandler fileHandler;
    private Chef chef;

    public Controller(AdminInsert adminInsert, WaiterInsert waiterInsert,AdministratorView administratorView, WaiterView waiterView, ChooseView chooseView, Restaurant restaurant,Chef chef,FileHandler fileHandler){
        this.chef=chef;
        this.fileHandler=fileHandler;
        this.restaurant=restaurant;
        this.adminInsert=adminInsert;
        this.waiterInsert=waiterInsert;
        this.waiterInsert.addActionListenerBackWI(new butonBackInsertW());
        this.waiterInsert.addActionListenerCreate(new createOrder());
        this.adminInsert.addActionListenerBackAI(new backButtonInsert());
        this.adminInsert.addActionListenerConfirm(new butonConfirm());
        this.adminInsert.addActionListenerViewInsert(new butonViewInsert());
        this.administratorView=administratorView;
        this.waiterView=waiterView;
        this.waiterView.addActionListenerBackW(new backButtonWaiter());
        this.waiterView.addActionListenerInsert(new butonInsertWaiter());
        this.waiterView.addActionListenerViewAllW(new butonViewAllW());
        this.waiterView.addActionListenerComputeBill(new createBill());
        this.waiterInsert.addActionListenerViewProd(new viewItemsInOrder());
        this.administratorView.addActionListenerBackAV(new backButtonAdmin());
        this.administratorView.addActionListenerInsert(new butonInsertAdmin());
        this.administratorView.addActionListenerViewAllA(new butonViewAllA());
        this.administratorView.addActionListenerDeleteA(new butonDeleteAdmin());
        this.administratorView.addActionListenerEditA(new butonEditAdmin());
        this.chooseView=chooseView;
        this.chooseView.addActionListenerAdmin(new butonAdmin());
        this.chooseView.addActionListenerWaiter(new butonWaiter());
    }

    public class backButtonAdmin implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            administratorView.setVisible(false);
            chooseView.setVisible(true);
        }
    }

    public class backButtonWaiter implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            waiterView.setVisible(false);
            chooseView.setVisible(true);
        }
    }

    public class butonInsertWaiter implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            waiterView.setVisible(false);
            waiterInsert.setVisible(true);
        }
    }

    public class butonBackInsertW implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            waiterInsert.setVisible(false);
            waiterView.setVisible(true);
        }
    }

    public class backButtonInsert implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            administratorView.setVisible(true);
            adminInsert.setVisible(false);
        }
    }

    public class butonAdmin implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            chooseView.setVisible(false);
            administratorView.setVisible(true);
        }
    }

    public class butonWaiter implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            chooseView.setVisible(false);
            waiterView.setVisible(true);
        }
    }

    public class butonInsertAdmin implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            administratorView.setVisible(false);
            adminInsert.setVisible(true);
        }
    }

    public class butonViewInsert implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            adminInsert.removePane();
            adminInsert.afisTabela();
            adminInsert.getMainPanel().revalidate();
        }
    }

    public class butonDeleteAdmin implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            String nume=String.valueOf(administratorView.getjTable().getModel().getValueAt(administratorView.getjTable().getSelectedRow(), 0));
            restaurant.deleteFromRestaurant(nume);

        }
    }

    public class butonEditAdmin implements ActionListener{

        public void actionPerformed(ActionEvent e){
            String nume=String.valueOf(administratorView.getjTable().getValueAt(administratorView.getjTable().getSelectedRow(),0));
            int pret=Integer.parseInt(String.valueOf(administratorView.getjTable().getValueAt(administratorView.getjTable().getSelectedRow(),1)));
            restaurant.editInRestaurant(nume,pret);
        }
    }

    public class createBill implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            int oID=Integer.parseInt(waiterView.getjTable().getValueAt(waiterView.getjTable().getSelectedRow(),0).toString());
            int tID=Integer.parseInt(waiterView.getjTable().getValueAt(waiterView.getjTable().getSelectedRow(),1).toString());
            List<String> lines = Arrays.asList(waiterView.getjTable().getValueAt(waiterView.getjTable().getSelectedRow(),0).toString(),waiterView.getjTable().getValueAt(waiterView.getjTable().getSelectedRow(),1).toString(),waiterView.getjTable().getValueAt(waiterView.getjTable().getSelectedRow(),2).toString(),waiterView.getjTable().getValueAt(waiterView.getjTable().getSelectedRow(),3).toString(),waiterView.getjTable().getValueAt(waiterView.getjTable().getSelectedRow(),4).toString());
            Path file = Paths.get("order"+waiterView.getjTable().getValueAt(waiterView.getjTable().getSelectedRow(),0).toString()+waiterView.getjTable().getValueAt(waiterView.getjTable().getSelectedRow(),1).toString()+".txt");
            try {
                Files.write(file, lines, Charset.forName("UTF-8"));
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            Set<Order> aux=new HashSet<Order>();
            aux.addAll(Restaurant.orderInformation.keySet());
            for(Order order:aux){
                if(order.getTableID()==tID && order.getOrderID()==oID){
                    Restaurant.orderInformation.remove(order);
                }
            }
            fileHandler.updateHashMap();
        }
    }

    public class viewItemsInOrder implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            waiterInsert.removePane();
            waiterInsert.afisTabela();
            waiterInsert.getMainPanel().revalidate();
        }
    }

    public class createOrder implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            int orderID=Integer.parseInt(waiterInsert.getOrderID().getText());
            int tableID=Integer.parseInt(waiterInsert.getTableID().getText());
            Date date=Searcher.parseDate(waiterInsert.getDate().getText());
            ArrayList<String> siruri=new ArrayList<String>();
            if(adminInsert.getjTable().getSelectedRows().length>=0){
                int[] selectedrows = waiterInsert.getjTable().getSelectedRows();
                for (int i = 0; i < selectedrows.length; i++) {
                    siruri.add(waiterInsert.getjTable().getValueAt(selectedrows[i], 0).toString());

                }
                if(siruri.size()>0) {
                    restaurant.createOrder(orderID, date, tableID, siruri);
                }
            }
        }
    }

    public class butonConfirm implements ActionListener{

        public void actionPerformed(ActionEvent e) {

            if(Restaurant.menuItems.size()<restaurant.getLimit()){
                String nume=adminInsert.getNume().getText();
                String componente=adminInsert.getComponenete().getText();
                if(componente.equals("Base")){
                    int pret=Integer.parseInt(adminInsert.getPret().getText());
                    restaurant.addInRestaurant(nume,pret,componente,null);
                }else{
                    ArrayList<String> siruri=new ArrayList<String>();
                    if(adminInsert.getjTable().getSelectedRows().length>1){
                        int[] selectedrows = adminInsert.getjTable().getSelectedRows();
                        int pret=0;
                        for (int i = 0; i < selectedrows.length; i++) {
                            pret+=restaurant.getPriceByName(adminInsert.getjTable().getValueAt(selectedrows[i], 0).toString());
                            siruri.add(adminInsert.getjTable().getValueAt(selectedrows[i], 0).toString());

                        }
                        restaurant.addInRestaurant(nume,pret,"",siruri);
                    }
                }
            }
        }
    }

    public class butonViewAllA implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            administratorView.removePane();
            administratorView.afisTabela();
            administratorView.getMainPanel().revalidate();

        }
    }

    public class butonViewAllW implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            waiterView.removePane();
            waiterView.afisTabela();
            waiterView.getMainPanel().revalidate();
        }
    }
}
