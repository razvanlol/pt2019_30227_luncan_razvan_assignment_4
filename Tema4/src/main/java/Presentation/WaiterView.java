package Presentation;

import Business.Restaurant;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class WaiterView extends JFrame {
    private JButton viewAll;
    private JButton insert;
    private JButton computeBill;
    private JButton backButton;

    private JPanel panelPentruButoane;
    private JPanel mainPanel;

    private JTable jTable;

    private JScrollPane jScrollPane;

    public WaiterView(){

        this.viewAll=new JButton("View All");
        this.insert=new JButton("Insert");
        this.computeBill=new JButton("Compute Bill");;
        this.backButton=new JButton("Back");

        this.jTable=new JTable();
        this.jScrollPane=new JScrollPane(jTable);

        this.panelPentruButoane=new JPanel();
        this.panelPentruButoane.setLayout(new BoxLayout(panelPentruButoane,BoxLayout.X_AXIS));

        this.mainPanel=new JPanel();
        this.mainPanel.setLayout(new GridLayout(2,1));

        this.panelPentruButoane.add(viewAll);
        this.panelPentruButoane.add(insert);
        this.panelPentruButoane.add(computeBill);
        this.panelPentruButoane.add(backButton);
        this.setPreferredSize(new Dimension(400,200));

        this.mainPanel.add(panelPentruButoane);

        this.setTitle("Waiter");
        this.setVisible(false);
        this.setContentPane(mainPanel);
        this.pack();

    }

    public void afisTabela(){
        this.jTable=ClasaAfisTabel.createOrderTable(Restaurant.orderInformation.keySet());
        this.jScrollPane=new JScrollPane(jTable);
        this.mainPanel.add(jScrollPane);
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public JTable getjTable() {
        return jTable;
    }

    public void removePane(){
        this.mainPanel.remove(this.jScrollPane);
    }

    public void addActionListenerInsert(ActionListener a){
        insert.addActionListener(a);
    }

    public void addActionListenerBackW(ActionListener a){
        backButton.addActionListener(a);
    }

    public void addActionListenerViewAllW(ActionListener a){viewAll.addActionListener(a);}

    public void addActionListenerComputeBill(ActionListener a){computeBill.addActionListener(a);}


}
