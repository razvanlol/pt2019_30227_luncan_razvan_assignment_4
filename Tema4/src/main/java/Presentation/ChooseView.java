package Presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class ChooseView extends JFrame {

    private JButton administrator;
    private JButton waiter;

    private JPanel panel;

    public ChooseView(){

        this.administrator=new JButton("Administrator");
        this.waiter=new JButton("Waiter");

        this.panel=new JPanel();

        this.panel.setLayout(new GridLayout(2,1));

        this.panel.add(administrator);
        this.panel.add(waiter);
        this.setTitle("Restaurant");
        this.setPreferredSize(new Dimension(400,100));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        this.setContentPane(panel);
        this.pack();

    }

    public void addActionListenerAdmin(ActionListener a){
        this.administrator.addActionListener(a);
    }

    public void addActionListenerWaiter(ActionListener a){this.waiter.addActionListener(a);}

}
