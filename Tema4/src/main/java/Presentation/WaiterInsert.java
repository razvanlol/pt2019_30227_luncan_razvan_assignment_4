package Presentation;

import Business.Restaurant;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class WaiterInsert extends JFrame {

    private JButton create;
    private JButton backButton;
    private JButton viewMenuItems;

    private JTextField orderID;
    private JTextField tableID;
    private JTextField date;

    private JPanel panelPentruTF;
    private JPanel panelPentruButoane;
    private JPanel mainPanel;

    private JTable jTable;

    private JScrollPane jScrollPane;

    public WaiterInsert(){

        this.backButton=new JButton("Back");
        this.create=new JButton("Create");
        this.viewMenuItems=new JButton("View Items");

        this.orderID=new JTextField("Order ID");
        this.tableID=new JTextField("Table ID");
        this.date=new JTextField("Date");

        this.panelPentruTF=new JPanel();
        this.panelPentruTF.setLayout(new GridLayout(4,1));

        this.panelPentruTF.add(orderID);
        this.panelPentruTF.add(tableID);
        this.panelPentruTF.add(date);

        this.panelPentruButoane=new JPanel();
        this.panelPentruButoane.setLayout(new GridLayout(1,3));

        this.panelPentruButoane.add(create);
        this.panelPentruButoane.add(backButton);
        this.panelPentruButoane.add(viewMenuItems);

        this.jTable=new JTable();
        this.jScrollPane=new JScrollPane(jTable);

        this.mainPanel=new JPanel();
        this.mainPanel.setLayout(new GridLayout(3,1));

        this.mainPanel.add(panelPentruButoane);
        this.mainPanel.add(panelPentruTF);
        this.mainPanel.add(jScrollPane);



        this.setTitle("Waiter Create Order");
        this.setPreferredSize(new Dimension(500,200));
        this.setVisible(false);
        this.setContentPane(mainPanel);
        this.pack();
    }

    public JTextField getOrderID() {
        return orderID;
    }

    public JTextField getTableID() {
        return tableID;
    }

    public JTextField getDate() {
        return date;
    }

    public void afisTabela(){
        this.jTable=ClasaAfisTabel.createTableInsert(Restaurant.menuItems);
        this.jScrollPane=new JScrollPane(jTable);
        this.mainPanel.add(jScrollPane);
    }

    public void removePane(){
        this.mainPanel.remove(this.jScrollPane);
    }


    public JPanel getMainPanel(){return mainPanel;}

    public JTable getjTable(){return  jTable;}

    public void addActionListenerBackWI(ActionListener a){
        backButton.addActionListener(a);
    }

    public void addActionListenerCreate(ActionListener a){create.addActionListener(a);
    }

    public void addActionListenerViewProd(ActionListener a){viewMenuItems.addActionListener(a);}

}
