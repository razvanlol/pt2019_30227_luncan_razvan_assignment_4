package Presentation;

import Business.Restaurant;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class AdministratorView extends JFrame {
    private JButton viewAll;
    private JButton insert;
    private JButton delete;
    private JButton edit;
    private JButton backButton;

    private JPanel panelPentruButoane;
    private JPanel mainPanel;

    private JTable jTable;

    private JScrollPane jScrollPane;

    public AdministratorView(){

        this.viewAll=new JButton("View All");
        this.insert=new JButton("Insert");
        this.delete=new JButton("Delete");
        this.edit=new JButton("Edit");
        this.backButton=new JButton("Back");

        this.jTable=new JTable();
        this.jScrollPane=new JScrollPane(jTable);

        this.panelPentruButoane=new JPanel();
        this.panelPentruButoane.setLayout(new BoxLayout(panelPentruButoane,BoxLayout.X_AXIS));

        this.mainPanel=new JPanel();
        this.mainPanel.setLayout(new GridLayout(2,1));

        this.panelPentruButoane.add(viewAll);
        this.panelPentruButoane.add(insert);
        this.panelPentruButoane.add(delete);
        this.panelPentruButoane.add(edit);
        this.panelPentruButoane.add(backButton);
        this.setPreferredSize(new Dimension(400,200));

        this.mainPanel.add(panelPentruButoane);

        this.setTitle("Administrator");
        this.setVisible(false);
        this.setContentPane(mainPanel);
        this.pack();

    }

    public void afisTabela(){
        this.jTable=ClasaAfisTabel.createTable(Restaurant.menuItems);
        this.jScrollPane=new JScrollPane(jTable);
        this.mainPanel.add(jScrollPane);
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public JTable getjTable() {
        return jTable;
    }

    public void removePane(){
        this.mainPanel.remove(this.jScrollPane);
    }

    public void addActionListenerInsert(ActionListener a){
        insert.addActionListener(a);
    }

    public void addActionListenerBackAV(ActionListener a){
        backButton.addActionListener(a);
    }

    public void addActionListenerViewAllA(ActionListener a){viewAll.addActionListener(a);}

    public void addActionListenerDeleteA(ActionListener a){delete.addActionListener(a);}

    public void addActionListenerEditA(ActionListener a){edit.addActionListener(a);}

}
