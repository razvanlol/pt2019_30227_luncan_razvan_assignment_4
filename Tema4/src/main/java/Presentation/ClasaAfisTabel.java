package Presentation;

import Business.MenuItem;
import Business.Order;
import Business.Restaurant;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Set;

public class ClasaAfisTabel {
    public static JTable createTable(ArrayList<MenuItem> lista){
        Object[][] obj=new Object[lista.size()][2];
        String[] columnName = new String[]{"Nume", "Pret"};
        int i=0;
        for(MenuItem menuItem:lista){
            obj[i][0]=menuItem.getName();
            obj[i][1]=menuItem.getPrice();
            i++;
        }
        JTable table=new JTable(obj,columnName);
        return table;
    }

    public static JTable createOrderTable(Set<Order> lista){
        Object[][] obj=new Object[lista.size()][5];
        String[] columnName=new String[]{"Order ID","Table ID","Date","Price","Items"};
        int i=0;
        for(Order order:lista){
            obj[i][0]=order.getOrderID();
            obj[i][1]=order.getTableID();
            obj[i][2]=order.getDate();
            obj[i][3]=Restaurant.computePrice(Restaurant.orderInformation.get(order));
            String sir="";
            for(MenuItem menuItem:Restaurant.orderInformation.get(order)){
                sir+=menuItem.getName()+" ";
            }
            obj[i][4]=sir;
            i++;
        }
        JTable table=new JTable(obj,columnName);
        return table;
    }

    public static JTable createTableInsert(ArrayList<MenuItem> lista){
        Object[][] obj=new Object[lista.size()][2];
        String[] columnName = new String[]{"Nume", "Pret"};
        int i=0;
        for(MenuItem menuItem:lista){
            obj[i][0]=menuItem.getName();
            obj[i][1]=menuItem.getPrice();
            i++;
        }
        JTable table=new JTable(obj,columnName);
        table.setRowSelectionAllowed(true);
        table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        return table;
    }
}
