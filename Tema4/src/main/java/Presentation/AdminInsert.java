package Presentation;

import Business.Restaurant;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class AdminInsert extends JFrame {

    private JButton confirm;
    private JButton backButton;
    private JButton viewProducts;

    private JTextField nume;
    private JTextField pret;
    private JTextField componenete;

    private JPanel panelPentruTF;
    private JPanel panelPentruButoane;
    private JPanel mainPanel;

    private JTable jTable;

    private JScrollPane jScrollPane;

    public AdminInsert(){

        this.backButton=new JButton("Back");
        this.confirm=new JButton("Confirm");
        this.viewProducts=new JButton("View Products");

        this.nume=new JTextField("Nume");
        this.pret=new JTextField("Pret");
        this.componenete=new JTextField("Componente");

        this.panelPentruTF=new JPanel();
        this.panelPentruTF.setLayout(new GridLayout(3,1));

        this.panelPentruTF.add(nume);
        this.panelPentruTF.add(pret);
        this.panelPentruTF.add(componenete);

        this.panelPentruButoane=new JPanel();
        this.panelPentruButoane.setLayout(new GridLayout(1,3));

        this.panelPentruButoane.add(confirm);
        this.panelPentruButoane.add(backButton);
        this.panelPentruButoane.add(viewProducts);

        this.mainPanel=new JPanel();
        this.mainPanel.setLayout(new GridLayout(3,1));

        this.jTable=new JTable();
        this.jScrollPane=new JScrollPane(jTable);

        this.mainPanel.add(panelPentruButoane);
        this.mainPanel.add(panelPentruTF);
        this.mainPanel.add(jScrollPane);



        this.setTitle("Admin Insert Menu Item");
        this.setPreferredSize(new Dimension(500,200));
        this.setVisible(false);
        this.setContentPane(mainPanel);
        this.pack();
    }



    public JTextField getNume() {
        return nume;
    }

    public JTextField getPret() {
        return pret;
    }

    public JTextField getComponenete() {
        return componenete;
    }

    public void afisTabela(){
        this.jTable=ClasaAfisTabel.createTableInsert(Restaurant.menuItems);
        this.jScrollPane=new JScrollPane(jTable);
        this.mainPanel.add(jScrollPane);
    }

    public void removePane(){
        this.mainPanel.remove(this.jScrollPane);
    }

    public JPanel getMainPanel(){return mainPanel;}

    public JTable getjTable(){return  jTable;}

    public void addActionListenerBackAI(ActionListener a){
        backButton.addActionListener(a);
    }

    public void addActionListenerConfirm(ActionListener a){confirm.addActionListener(a); }

    public void addActionListenerViewInsert(ActionListener a){viewProducts.addActionListener(a);}

}
