package Presentation;

import Business.Order;

import javax.swing.*;
import java.util.Observable;
import java.util.Observer;

public class Chef implements Observer {

    private JFrame jFrame;

    public Chef() {
        this.jFrame = new JFrame();
    }

    public void update(Observable o, Object arg) {
        JOptionPane.showMessageDialog(jFrame,"A new order with id "+((Order)arg).getOrderID()+" for the table with id "+((Order)arg).getTableID()+" has been made.");
    }
}
